const OAuth2Server = require('oauth2-server'),
    Request = OAuth2Server.Request,
    Response = OAuth2Server.Response;

const { app } = require('./server');

module.exports = {
    obtainToken: function (req, res) {

        var request = new Request(req);
        var response = new Response(res);

        return app.oauth.token(request, response)
            .then(function (token) {

                res.json(token);
            }).catch(function (err) {

                res.status(err.code || 500).json(err);
            });
    },
    authenticateRequest: function (req, res) {

        var request = new Request(req);
        var response = new Response(res);

        return app.oauth.token(request, response)
            .then(function (token) {

                res.json(token);
            }).catch(function (err) {

                res.status(err.code || 500).json(err);
            });
    },

    authenticateRequest: function (req, res, next) {
        console.log(req);

        var request = new Request(req);
        var response = new Response(res);

        return app.oauth.authenticate(request, response)
            .then(function (token) {

                next();
            }).catch(function (err) {

                res.status(err.code || 500).json(err);
            });
    }
};

app.oauth = new OAuth2Server({
    model: require('./mongo/auth'),
    accessTokenLifetime: 60 * 60,
    allowBearerTokensInQueryString: true
});
