<?php

namespace app\commands;

use Yii;
use yii\console\Controller;
use yii\console\ExitCode;
use yii\mongodb\Query;

class InsertController extends Controller
{
    public function actionIndex()
    {
        $query = new Query();
        $query->select(['id', 'username'])
            ->from('users')
            ->limit(10);
        $rows = $query->all();
        // $collection = $this->mongodb->getCollection('users');
        // $collection->insert(['username' => 'JohnSmith', 'password' => 21231231231]);
  
        echo " Registro insertado correctamente" . "\n";

        return ExitCode::OK;
    }
}
