module.exports = {
    state: { type: Number, default: 0, required: true },
    title: { type: String, required: true },
    color: { type: String, required: true },
    date_insert: { type: Date, default: Date.now, required: true },
    date_finish: { type: Date },
};