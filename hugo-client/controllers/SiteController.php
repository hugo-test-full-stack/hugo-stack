<?php

namespace app\controllers;

use Yii;
use yii\web\Response;
use yii\web\Controller;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Login;
use app\models\Order;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\mongodb\Query;
use yii\widgets\ActiveForm;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
            'auth' => [

                'class' => 'yii\authclient\AuthAction',

                // 'successCallback' => [$this, 'successCallback'],

                // 'successUrl' => $this->successUrl,
            ],
        ];
    }


    public function actionIndex()
    {
        if (!AuthController::auth()) {
            $model = new Login;
            $msg = null;
            
            if ($model->load(Yii::$app->request->post()) && Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
            
            if ($model->load(Yii::$app->request->post())) {
                if ($model->validate()) {
                    if (AuthController::login($model->username, $model->password)) {
                        $this->layout = 'main';
                        return $this->render("welcome");
                    } else {
                        $msg = "Credenciales incorrectas";
                        $model->username = null;
                        $model->password = null;
                    }
                } else {
                    $model->getErrors();
                }
            }
            $this->layout = 'login';
            return $this->render("auth", ['model' => $model, 'msg' => $msg]);
        } else {
            return $this->render("welcome");
        }
    }


    public function actionDestroy()
    {
        AuthController::reset();
        return $this->actionIndex();
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogin()
    {
        return $this->actionIndex();
    }

    /**
     * Displays orders page.
     *
     * @return string
     */
    public function actionOrders()
    {
        if (!AuthController::auth()) {
            return $this->actionIndex();
        } else {
            $model = new Order;
            $msg = null;
            
            if ($model->load(Yii::$app->request->post()) && Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
            
            if ($model->load(Yii::$app->request->post())) {
                if ($model->validate()) {
                    OrderController::store($model);
                    $msg = "Ordern agregada correctamente";
                    $model->title = null;
                    $model->color = null;
                } else {
                    $model->getErrors();
                }
            }
            $this->layout = 'main';
            return $this->render("orders", [
                'orders' => OrderController::index(),
                'model' => $model,
                'msg' => $msg
            ]);
        }
    }

    public function actionOrderlist()
    {
        if (!AuthController::auth()) {
            return $this->actionIndex();
        } else {
            $this->layout = 'main';
            return $this->render("orderlist", [
                'orders' => OrderController::index(),
            ]);
        }
    }

    public function actionOrderall()
    {
        if (!AuthController::auth()) {
            return $this->actionIndex();
        } else {
            $this->layout = 'main';
            return $this->render("orderall", [
                'orders' => OrderController::all(),
            ]);
        }
    }

    public function actionComando()
    {
        CommandController::command();
        return $this->render("command");
    }

    public function actionWelcome()
    {
        return $this->render("welcome");
    }

    public function actionCompleteorder()
    {
        OrderController::actionCompleteOrder($_GET);
    }

    public function actionSaluda()
    {
        OrderController::actionCompleteOrder();

        var_dump("hola mundo");
        die();

        // $query = new Query();
        // // compose the query
        // $query->select(['id', 'username'])
        //     ->from('users')
        //     ->limit(100);
        // // execute the query
        // $rows = $query->all();

        return $this->render("saluda");
    }
}
