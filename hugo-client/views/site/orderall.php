<?php

/* @var $this yii\web\View */


use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\controllers\SiteController;

$this->title = 'Ordenes lista';
$this->params['breadcrumbs'][] = $this->title;
?>

<h1><?= Html::encode($this->title) ?></h1>

<div class="orders">
    <table class="table">
        <thead>
            <tr>
                <th>Estado</th>
                <th>Nombre</th>
                <th>Fecha</th>
                <th>Finalizado</th>
                <th>Color</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($orders as $key => $value): ?>
                <tr>
                    <td><?= $value["state"] ?></td>
                    <td><?= $value["title"] ?></td>
                    <td><?= $value["date_insert"] ?></td>
                    <td>
                        <?= $value["date_finish"] ?>
                    </td>
                    <td>
                        <div class="orders__color" style="background:<?= $value['color'] ?>"></div>
                    </td>
                </tr>
            <?php endforeach ?>
        </tbody>
    </table>
</div>

<script>
setInterval(function(){ window.location = "?r=site/orderlist"; }, 10000);
</script>