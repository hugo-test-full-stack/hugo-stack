<?php

/* @var $this yii\web\View */


use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\controllers\SiteController;

$this->title = 'Ordenes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="orders__form">
    <h1><?= Html::encode($this->title) ?></h1>

    <?php
        $form = ActiveForm::begin([
        "method" => "post",
        "id" => "orders",
        "enableClientValidation" => false,
        "enableAjaxValidation" => true,
    ]);
    ?>

        <div class="form-group">
            <?= $form->field($model, "title")->input("text") ?>
        </div>
        <div class="form-group">
            <?php $var = [ '#ea4a4a' => 'Rojo', '#7bb75f' => 'Verde',  '#e4e351' => 'Amarillo']; ?>
            <?= $form->field($model, 'color')->dropDownList($var, ['prompt' => 'Seleccione Uno' ]); ?>
        </div>

        



        <div class="">
            <?= Html::submitButton("Enviar", ["class" => "btn btn-primary"]) ?>
        </div>
        <div class="orders__form__message">
            <?= $msg ?>
        </div>


    <?php ActiveForm::end(); ?>

</div>
