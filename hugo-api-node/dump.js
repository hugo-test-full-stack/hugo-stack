const { mongo } = require('./mongo');
/**
 * Configuration.
 */

var clientModel = require('./mongo/model/client'),
    tokenModel = require('./mongo/model/token'),
    userModel = require('./mongo/model/user');

/**
 * Add example client and user to the database (for debug).
 */

var loadExampleData = function () {

    var client1 = new clientModel({
        clientId: 'application',
        clientSecret: 'secret',
        grants: [
            'password'
        ],
        redirectUris: []
    });
    var user = new userModel({
        id: '123',
        username: 'admin',
        password: 'admin'
    });

    client1.save(function (err, client) {

        if (err) {
            return console.error(err);
        }
        console.log('Created client', client);
    });

    user.save(function (err, user) {

        if (err) {
            return console.error(err);
        }
        console.log('Created user', user);
    });
};

/**
 * Dump the database content (for debug).
 */

var dump = function () {

    clientModel.find(function (err, clients) {

        if (err) {
            return console.error(err);
        }
        console.log('clients', clients);
    });

    tokenModel.find(function (err, tokens) {

        if (err) {
            return console.error(err);
        }
        console.log('tokens', tokens);
    });

    userModel.find(function (err, users) {

        if (err) {
            return console.error(err);
        }
        console.log('users', users);
    });
};

loadExampleData();
dump();
// process.exit();