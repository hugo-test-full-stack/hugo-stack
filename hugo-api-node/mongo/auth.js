const { mongo } = require('../mongo');
/**
 * Configuration.
 */

var clientModel = require('./model/client'),
    tokenModel = require('./model/token'),
    userModel = require('./model/user');

/*
 * Methods used by all grant types.
 */

var getAccessToken = function (token) {

    return tokenModel.findOne({
        accessToken: token
    });
};

var getClient = function (clientId, clientSecret) {

    return clientModel.findOne({
        clientId: clientId,
        clientSecret: clientSecret
    });
};

var saveToken = function (token, client, user) {

    token.client = {
        id: client.clientId
    };

    token.user = {
        id: user.username || user.clientId
    };

    var tokenInstance = new tokenModel(token);

    tokenInstance.save();

    return token;
};

/*
 * Method used only by password grant type.
 */

var getUser = function (username, password) {

    return userModel.findOne({
        username: username,
        password: password
    });
};

/*
 * Method used only by client_credentials grant type.
 */

var getUserFromClient = function (client) {

    return clientModel.findOne({
        clientId: client.clientId,
        clientSecret: client.clientSecret,
        grants: 'client_credentials'
    });
};

/**
 * Export model definition object.
 */

module.exports = {
    getAccessToken: getAccessToken,
    getClient: getClient,
    saveToken: saveToken,
    getUser: getUser,
    getUserFromClient: getUserFromClient
};