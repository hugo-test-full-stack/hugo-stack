<?php

namespace app\models;

use Yii;
use yii\base\Model;

class Login extends Model
{
    public $username;
    public $password;
    
    public function rules()
    {
        return [
            ['username', 'required', 'message' => 'Campo requerido'],
            ['username', 'match', 'pattern' => "/^.{5,50}$/", 'message' => 'Mínimo 5 y máximo 50 caracteres'],
            ['username', 'match', 'pattern' => "/^[0-9a-z]+$/i", 'message' => 'Sólo se aceptan letras y números'],
            ['password', 'required', 'message' => 'Campo requerido'],
        ];
    }
    
    public function attributeLabels()
    {
        return [
            'username' => 'Username:',
            'password' => 'Password:',
        ];
    }
}
