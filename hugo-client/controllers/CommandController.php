<?php
namespace app\controllers;

use Yii;
use DateTime;
use Exception;
use yii\web\Controller;

class CommandController extends Controller
{
    public function command()
    {
        $colors = [ '#ea4a4a' ,'#7bb75f' ,'#e4e351'];
        $collection = Yii::$app->mongodb->getCollection('orders');
        $collection->insert(
            [
                'statee' => 0,
                'title' => 'Orden #'.rand(1, 100),
                'color' => $colors[rand(0, count($colors) - 1)],
                'date_insert' => date("Y-m-d H:i:s"),
            ]
        );
    }
}
