var Order = require('../mongo/model/order');

exports.create = function (req, res, next) {
    var order = new Order(
        {
            state: req.body.state,
            title: req.body.title,
            color: req.body.color,
            date_insert: req.body.date_insert,
            date_finish: null,
        }
    );

    order.save(function (err) {
        if (err) {
            return next(err);
        }
        res.send('order Created successfully')
    })
};

exports.details = function (req, res, next) {
    Order.findById(req.params.id, function (err, order) {
        if (err) return next(err);
        res.send(order);
    })
};

exports.all = function (req, res, next) {
    Order.find({}, function (err, order) {
        if (err) return next(err);
        res.send(order);
    })
};

exports.update = function (req, res, next) {
    Order.findByIdAndUpdate(req.params.id, { $set: req.body }, function (err, order) {
        if (err) return next(err);
        res.send('order udpated.');
    });
};

exports.delete = function (req, res, next) {
    Order.findByIdAndRemove(req.params.id, function (err) {
        if (err) return next(err);
        res.send('Deleted successfully!');
    })
};