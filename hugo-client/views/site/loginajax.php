<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">
    <img class="login__logo" src="img/logo-white.png" alt="">
<?php
    $form = ActiveForm::begin([
    "method" => "post",
    "id" => "formulario",
    "enableClientValidation" => false,
    "enableAjaxValidation" => true,
]);
?>

<div class="form-group">
    <?= $form->field($model, "username")->input("text") ?>
</div>
<div class="form-group">
    <?= $form->field($model, "password")->input("text") ?>
</div>

<?= Html::submitButton("Enviar", ["class" => "btn btn-primary"]) ?>


    <?php ActiveForm::end(); ?>

    <div class="" style="color:#999;">
        You may login with <strong>admin/admin</strong> or <strong>demo/demo</strong>.<br>
    </div>
</div>
