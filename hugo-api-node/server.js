express = require('express');

module.exports = {
    app: express(),
    router: express.Router(),
}
