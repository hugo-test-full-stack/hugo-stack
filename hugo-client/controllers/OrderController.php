<?php
namespace app\controllers;

use Yii;
use DateTime;
use Exception;
use yii\web\Controller;

class OrderController extends Controller
{
    public function index()
    {
        try {
            $response = Yii::$app->httpclient->get(
                getenv('API_URL').'/order/',
                [
                    'headers' => [
                        'Content-Type' => 'application/x-www-form-urlencoded',
                        'Authorization' => 'Bearer '.$_SESSION["authApi"]["accessToken"],
                    ],
                ]
            );

            $orders = [];

            foreach ($response as $key => $value) {
                if ($value["state"] === 0) {
                    $state = "No completado";
                } elseif ($value["state"] === 1) {
                    $state = "Completado";
                }
    
                $date_insert = $value["date_insert"];
                $date_insert = DateTime::createFromFormat('Y-m-d\TH:i:s+', $date_insert);
                $now = new DateTime(date("Y-m-d H:i:s"));
                $interval = $now->diff($date_insert);
    
                $title = $value["title"];
                $color = $value["color"];

                if ($interval->format("%Imin") > 10 && $value["state"] === 0) {
                    OrderController::completeOrder($value["_id"]);
                } elseif ($interval->format("%Imin") < 10 && $value["state"] === 0) {
                    $orders[] = [
                        'id' => $value["_id"],
                        'state' => $state,
                        'date_insert' => $date_insert->format("d/m/Y h:i:s A"),
                        'time' => $interval->format("%Imin")." ".$interval->format("%Ss"),
                        'title' => $title,
                        'color' => $color,
                    ];
                }
            }


                    

            return $orders;
        } catch (Exception $e) {
            // return $e->getMessage();
            return false;
        }
    }

    public function all()
    {
        try {
            $response = Yii::$app->httpclient->get(
                getenv('API_URL').'/order/',
                [
                    'headers' => [
                        'Content-Type' => 'application/x-www-form-urlencoded',
                        'Authorization' => 'Bearer '.$_SESSION["authApi"]["accessToken"],
                    ],
                ]
            );

            $orders = [];

            foreach ($response as $key => $value) {
                if ($value["state"] === 0) {
                    $state = "No completado";
                } elseif ($value["state"] === 1) {
                    $state = "Completado";
                }
    
                $date_insert = $value["date_insert"];
                $date_insert = DateTime::createFromFormat('Y-m-d\TH:i:s+', $date_insert);
                $date_finish = $value["date_finish"];
                $date_finish = DateTime::createFromFormat('Y-m-d\TH:i:s+', $date_finish);
                
                if ($date_finish) {
                    $date_finish = $date_finish->format("d/m/Y h:i:s A");
                } else {
                    $date_finish = "";
                }
    
                $title = $value["title"];
                $color = $value["color"];


                $orders[] = [
                    'id' => $value["_id"],
                    'state' => $state,
                    'date_insert' => $date_insert->format("d/m/Y h:i:s A"),
                    'title' => $title,
                    'color' => $color,
                    'date_finish' => $date_finish,
                ];
            }


                    

            return $orders;
        } catch (Exception $e) {
            // return $e->getMessage();
            return false;
        }
    }

    public function store($model)
    {
        try {
            $response = Yii::$app->httpclient->post(
                getenv('API_URL').'/order/create',
                [
                    'title' => $model->title,
                    'color' => $model->color,
                ],
                [
                    'headers' => [
                        'Content-Type' => 'application/x-www-form-urlencoded',
                        'Authorization' => 'Bearer '.$_SESSION["authApi"]["accessToken"],
                    ],
                ]
            );

            return true;
        } catch (Exception $e) {
            // return $e->getMessage();
            return false;
        }
    }

    public function actionCompleteOrder($data)
    {
        OrderController::completeOrder($data["id"]);
    }

    public function completeOrder($id)
    {
        $response = Yii::$app->httpclient->put(
            getenv('API_URL').'/order/'.$id.'/update',
            [
                'state' => 1,
                'date_finish' => date("Y-m-d H:i:s"),
            ],
            [
                'headers' => [
                    'Content-Type' => 'application/x-www-form-urlencoded',
                    'Authorization' => 'Bearer '.$_SESSION["authApi"]["accessToken"],
                ],
            ]
        );
    }
}
