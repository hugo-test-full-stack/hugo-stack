function completeOrder(id) {
    $.ajax({
        url: '?r=site/completeorder',
        data: { id: id },
        success: function (respuesta) {
            window.location = "?r=site/orderlist";
        },
        error: function () {
            console.log("No se ha podido obtener la información");
        }
    });
}