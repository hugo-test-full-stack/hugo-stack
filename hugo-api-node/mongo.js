const mongoose = require('mongoose');
const { mongodb_url } = require('./config');

module.exports = {
    mongodb: mongoose.connect(mongodb_url, {
        useCreateIndex: true,
        useNewUrlParser: true
    }, function (err, res) {

        if (err) {
            return console.error('Error connecting to "%s":', mongodb_url, err);
        }
        console.log('Connected successfully to "%s"', mongodb_url);
    })
};
