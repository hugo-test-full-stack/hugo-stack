const { app, router } = require('../server');

var order_controller = require('./../controllers/order');

router.get('/', order_controller.all);
router.post('/create', order_controller.create);
router.get('/:id', order_controller.details);
router.put('/:id/update', order_controller.update);
router.delete('/:id/delete', order_controller.delete);

module.exports = router;