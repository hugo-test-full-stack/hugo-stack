const { app, router } = require('./server');
const bodyParser = require('body-parser');
const order = require('./routes/order');
const listEndpoints = require('express-list-endpoints')
const auth = require('./auth');


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.all('/oauth/token', auth.obtainToken);
app.get('/', auth.authenticateRequest, function (req, res) {

    res.send('middleware auth');
});
app.use('/order', auth.authenticateRequest, order);

console.log(listEndpoints(app));

var port = 3000;

app.listen(port, () => {
    console.log('Server is up and running on port numner ' + port);
});