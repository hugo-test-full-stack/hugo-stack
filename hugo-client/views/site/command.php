<?php

/* @var $this yii\web\View */


use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\controllers\SiteController;

$this->title = 'Registro insertado - MongoDB';
$this->params['breadcrumbs'][] = $this->title;
?>

<h1><?= Html::encode($this->title) ?></h1>

<div class="orders">
    <p>Se acaba de ejecutar un insert directamente a la base de datos mongoDB 🤩</p>
</div>

