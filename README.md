## 🔥 Prueba de hugo

Login:

![alt text](https://i.imgur.com/cZFCMo7.png)

Panel:

![alt text](https://i.imgur.com/ANhTemv.png)
![alt text](https://i.imgur.com/FHSJuHD.png)

## 🚀 Install

Solo se necesita tener docker instalado y ejecutar estos comandos:

```sh
docker-compose build
```

```sh
docker-compose up -d
```

```sh
docker-compose restart api
```

Luego, hay que entrar a la consola de node para poder ejecutar un pequeño DUMP:

```sh
 docker-compose exec api sh
```

Ya en la consola, hay que ejecutar el archivo `dump.js`:

```sh
 node dump.js
```

Y listo ✨, el cliente de Yii2 se está ejecutando en `localhost:8080`,  las credenciales de prueba se encuentran en el login.

La API de node se ejecuta en `localhost:3000` y dejo la [colección](https://www.getpostman.com/collections/88622cce9809896f8732) de PostMan de los endpoints.
