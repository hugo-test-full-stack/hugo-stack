<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <link rel="icon" type="image/png" href="img/favicon.png" />
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,700&display=swap" rel="stylesheet">

</head>
<body style="background:#38215c">
<?php $this->beginBody() ?>

<div class="login">
    <div class="container">
        <?= $content ?>
    </div>
</div>

<div style="position: absolute;width: 100%;bottom: 0;">
    <footer class="footer">
        <img class="circle-shape" src="img/circle-shape.png" alt="">
        <img class="orange-shape" src="img/orange-shape.png" alt="">
        <div class="container">
            <p class="make-with-love">Hecho con ❤️ por  <a href="https://www.linkedin.com/in/carlos-monge-a8a45436/" target="_blank"><strong> Carlos Monge</strong></a></p>
        </div>
    </footer>
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
