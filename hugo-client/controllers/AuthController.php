<?php
namespace app\controllers;

use Yii;
use Exception;
use yii\web\Controller;

class AuthController extends Controller
{
    public function login($username, $password)
    {
        try {
            $json = Yii::$app->httpclient->post(
                getenv('API_URL').'/oauth/token',
                [
                    'username' => $username,
                    'password' => $password,
                    'grant_type' => 'password',
                ],
                [
                    'headers' => [
                        'Content-Type' => 'application/x-www-form-urlencoded',
                        'Authorization' => 'Basic '. base64_encode("application:secret"),
                    ],
                ]
            );
            $_SESSION["authApi"] = $json;
            $response = Yii::$app->httpclient->get(
                getenv('API_URL'),
                [
                    'headers' => [
                        'Content-Type' => 'application/x-www-form-urlencoded',
                        'Authorization' => 'Bearer '.$_SESSION["authApi"]["accessToken"],
                    ],
                ]
            );

            return true;
        } catch (Exception $e) {
            // return $e->getMessage();
            return false;
        }
    }

    public function auth()
    {
        try {
            $response = Yii::$app->httpclient->get(
                getenv('API_URL'),
                [
                    'headers' => [
                        'Content-Type' => 'application/x-www-form-urlencoded',
                        'Authorization' => 'Bearer '.$_SESSION["authApi"]["accessToken"],
                    ],
                ]
            );
        } catch (Exception $e) {
            return false;
        }

        if ($response === 'middleware auth') {
            return true;
        } else {
            return false;
        }
    }

    public function reset()
    {
        unset($_SESSION["authApi"]);
        return true;
    }
}
