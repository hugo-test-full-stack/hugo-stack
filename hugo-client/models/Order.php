<?php

namespace app\models;

use Yii;
use yii\base\Model;

class Order extends Model
{
    public $state;
    public $title;
    public $color;
    public $date_insert;
    public $date_finish;

    public function rules()
    {
        return [
            ['title', 'required', 'message' => 'Campo requerido'],
            ['color', 'required', 'message' => 'Campo requerido'],
        ];
    }
    
    public function attributeLabels()
    {
        return [
            'title' => 'Nombre de la orden:',
            'color' => 'color:',
        ];
    }
}
